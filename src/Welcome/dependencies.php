<?php
// include after instantiating the container in the bootstrap file


$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
		$view = $container->get('view');
        $locale = $container->get('locale');
        $content = $view->render('error', [
            'theme'=>'default',
            'message'=>$locale->gettext('ERROR_404')
        ]);
        return $response
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write($content);
    };
};

$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
		$view = $container->get('view');
        $content = $view->render('error', [
            'theme'=>'default',
            'message'=>'500 Server Error',
            'details'=>$exception
        ]);
        return $container['response']
            ->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($content);
    };
};

$container['view'] = function ($container) {

	$dir = dirname(__FILE__);
	$view = new League\Plates\Engine($dir . '/Views');
    $view->addFolder('layouts', $dir . '/Layouts');
	$uri = $container->get('request')->getUri();
	$view->addData([
		'baseUri'=>$uri->getBasePath(),
		'title'=>'Your Application',
        'locale'=>$container->get('locale')
	]);
	$view->loadExtension(new \Welcome\Views\Extensions\Alerts($container));
	return $view;
};

$container['session'] = function () {
	$session_factory = new \Aura\Session\SessionFactory;
	return $session_factory->newInstance($_COOKIE);

};

$container['locale'] = function () {
    $locale = new \Vespula\Locale\Locale('en_CA');
    $locale->load(__DIR__ . '/Locales');
    return $locale;
};
