<?php /* Change the layout to bootstrap or foundation */ ?>

<?php $this->layout('layouts::' . $theme) ?>


<h2>Welcome</h2>

<p>Create your own project by running the command line tool and selecting 'p' for project.</p>

<pre>
$ ./script/create
</pre>

<p>Then, modify your index.php file in the html folder to point to your project instead of the Welcome project.</p>

<?php echo $this->info("Make sure you update your .htaccess file"); ?>
