<?php
// default route
$app->get('/', function ($request, $response) {
    $controller = new \Welcome\Controllers\Index($this, $request, $response);
    return $controller->index();
});

