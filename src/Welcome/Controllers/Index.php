<?php

namespace Welcome\Controllers;


class Index extends Controller {
	
	public function index()
	{
        $this->messages['info'][] = 'This is an info message';
        $this->messages['error'][] = 'This is an error message';
		
        
        $content = $this->view->render('index/index', [
            'controller'=>'Index', 
            'project'=>'Welcome',
            'messages'=>$this->messages,
            'theme'=>'default'
        ]);
        return $this->response->write($content);
	}
}
