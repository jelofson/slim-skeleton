    <head>
        <meta charset="utf-8">
        <!-- Web Experience Toolkit (WET) / Boîte à outils de l'expérience Web (BOEW)
        wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html -->
        <title><?=$this->e($title); ?></title>
        <meta content="width=device-width,initial-scale=1" name="viewport">
        <!-- Meta data -->
        <meta name="description" content="Web Experience Toolkit (WET) includes reusable components for building and maintaining innovative Web sites that are accessible, usable, and interoperable. These reusable components are open source software and free for use by departments and external Web communities">
        <!-- Meta data-->
        <!--[if gte IE 9 | !IE ]><!-->
        <link href="<?=$baseUri; ?>/gc/GCWeb/assets/favicon.ico" rel="icon" type="image/x-icon">
        <link rel="stylesheet" href="<?=$baseUri; ?>/gc/GCWeb/css/theme.min.css">
        <!--<![endif]-->
        <!--[if lt IE 9]>
        <link href="<?=$baseUri; ?>/gc/GCWeb/assets/favicon.ico" rel="shortcut icon" />

        <link rel="stylesheet" href="<?=$baseUri; ?>/gc/GCWeb/css/ie8-theme.min.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<?=$baseUri; ?>/gc/wet-boew/js/ie8-wet-boew.min.js"></script>
        <![endif]-->
        <!--[if lte IE 9]>


        <![endif]-->
        <noscript><link rel="stylesheet" href="<?=$baseUri; ?>/gc/wet-boew/css/noscript.min.css" /></noscript>
        <!-- Google Tag Manager DO NOT REMOVE OR MODIFY - NE PAS SUPPRIMER OU MODIFIER -->
        <script>dataLayer1 = [];</script>
        <!-- End Google Tag Manager -->
    </head>
