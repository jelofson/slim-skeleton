    	<footer role="contentinfo" id="wb-info" class="visible-sm visible-md visible-lg wb-navcurr">
            <div class="container">
                <nav role="navigation">
                    <h2><?php echo $locale->gettext('TEXT_ABOUT_SITE'); ?></h2>
                    <ul id="gc-tctr" class="list-inline">
                        <li><a rel="license" href="<?php echo $locale->gettext('URL_TERMS_CONDITIONS'); ?>"><?php echo $locale->gettext('TEXT_TERMS_CONDITIONS'); ?></a></li>
                        <li><a href="<?php echo $locale->gettext('URL_TRANSPARENCY'); ?>"><?php echo $locale->gettext('TEXT_TRANSPARENCY'); ?></a></li>
                    </ul>
                    <div class="row">
                        <section class="col-sm-3">
                            <h3><a href="<?php echo $locale->gettext('URL_ABOUT_US'); ?>"><?php echo $locale->gettext('TEXT_ABOUT_US'); ?></a></h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $locale->gettext('URL_OUR_DEPUTY'); ?>"><?php echo $locale->gettext('TEXT_OUR_DEPUTY'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_ACTS_REGS'); ?>"><?php echo $locale->gettext('TEXT_ACTS_REGS'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_CAREERS'); ?>"><?php echo $locale->gettext('TEXT_CAREERS'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_PUBLICATIONS_REPORTS'); ?>"><?php echo $locale->gettext('TEXT_PUBLICATIONS_REPORTS'); ?></a></li>
                            </ul>
                        </section>
                        <section class="col-sm-3">
                            <h3><a href="<?php echo $locale->gettext('URL_NEWS'); ?>"><?php echo $locale->gettext('TEXT_NEWS'); ?></a></h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $locale->gettext('URL_NEWS_RELEASES'); ?>"><?php echo $locale->gettext('TEXT_NEWS_RELEASES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_MEDIA_ADVISORIES'); ?>"><?php echo $locale->gettext('TEXT_MEDIA_ADVISORIES'); ?></a></li>
                                <li><a href="<?php echo $locale->gettext('URL_PHOTO_GALLERY'); ?>"><?php echo $locale->gettext('TEXT_PHOTO_GALLERY'); ?></a></li>
                            </ul>
                        </section>
                        <section class="col-sm-3">
                            <h3><a href="<?php echo $locale->gettext('URL_CONTACT_US'); ?>"><?php echo $locale->gettext('TEXT_CONTACT_US'); ?></a></h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $locale->gettext('URL_FIND_EMPLOYEE'); ?>"><?php echo $locale->gettext('TEXT_FIND_EMPLOYEE'); ?></a></li>
                            </ul>
                        </section>
                        <section class="col-sm-3">
                            <h3><a href="<?php echo $locale->gettext('URL_STAY_CONNECTED'); ?>"><?php echo $locale->gettext('TEXT_STAY_CONNECTED'); ?></a></h3>
                            <ul class="list-unstyled">
                            	<li><a href="<?php echo $locale->gettext('URL_TWITTER'); ?>">Twitter</a></li>
                            	<li><a href="<?php echo $locale->gettext('URL_YOUTUBE'); ?>">YouTube</a></li>
                            	<li><a href="<?php echo $locale->gettext('URL_FEEDS'); ?>"><?php echo $locale->gettext('TEXT_FEEDS'); ?></a></li>
                            </ul>
                        </section>
                    </div>
                </nav>
            </div>
            <div id="gc-info">
                <div class="container">
                    <nav role="navigation">
                        <h2><?php echo $locale->gettext('TEXT_GOC_FOOTER'); ?></h2>
                        <ul class="list-inline">
                            <li><a href="<?php echo $locale->gettext('URL_HEALTH'); ?>"><span><?php echo $locale->gettext('TEXT_HEALTH'); ?></span></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_TRAVEL'); ?>"><span><?php echo $locale->gettext('TEXT_TRAVEL'); ?></span></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_SERVICE_CANADA'); ?>"><span><?php echo $locale->gettext('TEXT_SERVICE_CANADA'); ?></span></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_JOBS'); ?>"><span><?php echo $locale->gettext('TEXT_JOBS'); ?></span></a></li>
                            <li><a href="<?php echo $locale->gettext('URL_ECONOMY'); ?>"><span><?php echo $locale->gettext('TEXT_ECONOMY'); ?></span></a></li>
                            <li id="canada-ca"><a href="<?php echo $locale->gettext('URL_CANADA_CA'); ?>">Canada.ca</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>