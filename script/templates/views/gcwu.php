<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie9" lang="<?php echo $locale->gettext('TEXT_LANG'); ?>" dir="ltr"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo $locale->gettext('TEXT_LANG'); ?>" dir="ltr">
<!--<![endif]-->
    
    <?php $this->insert('layouts::gcwu/head'); ?>
    
    <body vocab="http://schema.org/" typeof="WebPage">
        <ul id="wb-tphp">
            <li class="wb-slc">
                <a class="wb-sl" href="#wb-cont"><?php echo $locale->gettext('TEXT_SKIP_MAIN'); ?></a>
            </li>
            <li class="wb-slc visible-sm visible-md visible-lg">
                <a class="wb-sl" href="#wb-info"><?php echo $locale->gettext('TEXT_SKIP_ABOUT'); ?></a>
            </li>
        </ul>
        
        <?php $this->insert('layouts::gcwu/header'); ?>
        
        <main role="main" property="mainContentOfPage" class="container">
            <h1>Government of Canada Web Usability Layout</h1>
            <?=$this->section('content')?>
        </main>
        
        <?php $this->insert('layouts::gcwu/footer'); ?>
        
    <!--[if gte IE 9 | !IE ]><!-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="<?=$baseUri; ?>/gc/wet-boew/js/wet-boew.min.js"></script>
    <script src="<?=$baseUri; ?>/gc/wet-boew/js/alert.js"></script>
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="<?=$baseUri; ?>/gc/wet-boew/js/ie8-wet-boew2.min.js"></script>

    <![endif]-->
    <script src="<?=$baseUri; ?>/gc/theme-gcwu-fegc/js/theme.min.js"></script>
    </body>
</html>