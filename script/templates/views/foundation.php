<!DOCTYPE html>
<html class="no-js" lang="en">
    <?php $this->insert('layouts::foundation/head'); ?>
    <body>
        
        <?php $this->insert('layouts::foundation/navbar'); ?>
        
        <div class="row">
            <div class="large-12 columns">
                <h1>Foundation Layout</h1>
                <?=$this->section('content')?>
                
            </div>
        </div>

        <script src="<?=$baseUri; ?>/foundation/js/vendor/jquery.js"></script>
        <script src="<?=$baseUri; ?>/foundation/js/vendor/what-input.js"></script>
        <script src="<?=$baseUri; ?>/foundation/js/vendor/foundation.js"></script>
        <script src="<?=$baseUri; ?>/foundation/js/app.js"></script>
    </body>
</html>