<?php

namespace Cli;

class Project extends Base {
    
    
    public function build() 
    {
        // Does the project exist?
        if ($this->projectExists()) {
            $this->error('Project exists.');
            return;
        } 
        
        if ($this->createControllerFolder()) {
            $this->info('Project folder created successfully');
        } else {set_error_handler([$this, 'setWarning'], E_WARNING);
            $this->error('Project folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createViewFolder()) {
            $this->info('Project view folder created successfully');
        } else {
            $this->error('Project view folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createLayoutFolder()) {
            $this->info('Project layout folder created successfully');
        } else {
            $this->error('Project layout folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createLocaleFolder()) {
            $this->info('Project locale folder created successfully');
        } else {
            $this->error('Project locale folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createExtensionsFolder()) {
            $this->info('Project view extensions folder created successfully');
        } else {
            $this->error('Project view extensions folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createBaseControllerTemplate()) {
            $this->info('Base controller created in project folder');
        } else {
            $this->error('Base controller not created :( ' . $this->getWarning());
        }
        
        if ($this->createSharedLayout()) {
            $this->info('Project shared layout file created successfully');
        } else {
            $this->error('Project shared layout file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createErrorView()) {
            $this->info('Project error view file created successfully');
        } else {
            $this->error('Project error view file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createBaseExtension()) {
            $this->info('Project base view extension file created successfully');
        } else {
            $this->error('Project base view extension file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createAlertExtension()) {
            $this->info('Project alert view extension file created successfully');
        } else {
            $this->error('Project alert view extension file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->copyLocales()) {
            $this->info('Project locale files copied successfully');
        } else {
            $this->error('Project locale files not copies :( ' . $this->getWarning());
            return;
        }
        
        if ($this->copyAlertExtensionTemplates()) {
            $this->info('Project alert view extension templates copied successfully');
        } else {
            $this->error('Project alert view extension templates not copied :( ' . $this->getWarning());
            return;
        }

        if ($this->createDependencies()) {
            $this->info('Project dependencies file created successfully');
        } else {
            $this->error('Project dependencies file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createRoutes()) {
            $this->info('Project routes file created successfully');
        } else {
            $this->error('Project routes file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createMiddleware()) {
            $this->info('Project middleware file created successfully');
        } else {
            $this->error('Project middleware file not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createDefaultController()) {
            $this->info('Project default controller created successfully');
        } else {
            $this->error('Project default controller not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createControllerViewFolder()) {
            $this->info('Project default controller view folder created successfully');
        } else {
            $this->error('Project default controller view folder not created :( ' . $this->getWarning());
            return;
        }
        
        if ($this->createControllerIndexView()) {
            $this->info('Project default controller index view created successfully');
        } else {
            $this->error('Project default controller index view not created :( ' . $this->getWarning());
            return;
        }

        $this->build_status = self::BUILD_STATUS_OK;
        
    }
    
    protected function projectExists()
    {
        return file_exists($this->base_path . '/src/' . $this->project);
    }
    
    
    protected function createControllerFolder()
    {
        // Recursively create controller folder (creates project folder too)
        $controller_path = $this->base_path . '/src/' . $this->project . '/Controllers';
        return mkdir($controller_path, 0755, true);
    }
    
    protected function createViewFolder()
    {
        $view_path = $this->base_path . '/src/' . $this->project . '/Views';
        return mkdir($view_path, 0755);
    }
    
    protected function createLayoutFolder()
    {
        $view_path = $this->base_path . '/src/' . $this->project . '/Layouts';
        return mkdir($view_path, 0755);
    }
    
    protected function createLocaleFolder()
    {
        $locale_path = $this->base_path . '/src/' . $this->project . '/Locales';
        return mkdir($locale_path, 0755);
    }
    
    protected function createExtensionsFolder()
    {
        // create the alerts subfolder at the same time
        $ext_path = $this->base_path . '/src/' . $this->project . '/Views/Extensions/Alerts';
        return mkdir($ext_path, 0755, true);
    }
    
    protected function createBaseControllerTemplate()
    {
        $filename = $this->base_path . '/src/' . $this->project . '/Controllers/Controller.php'; 
        $template = file_get_contents($this->base_path . '/script/templates/base.php');
        $template = str_replace('{project}', $this->project, $template);
        // return num bytes written or FALSE
        return file_put_contents($filename, $template);

    }
    
    protected function createErrorView()
    {
        return copy($this->base_path . '/script/templates/views/error.php', $this->base_path . '/src/' . $this->project . '/Views/error.php');
    }
    
    protected function createSharedLayout()
    {
        copy($this->base_path . '/script/templates/views/default.php', $this->base_path . '/src/' . $this->project . '/Layouts/default.php');
        copy($this->base_path . '/script/templates/views/foundation.php', $this->base_path . '/src/' . $this->project . '/Layouts/foundation.php');
        copy($this->base_path . '/script/templates/views/bootstrap.php', $this->base_path . '/src/' . $this->project . '/Layouts/bootstrap.php');
        copy($this->base_path . '/script/templates/views/gcweb.php', $this->base_path . '/src/' . $this->project . '/Layouts/gcweb.php');
        copy($this->base_path . '/script/templates/views/gcwu.php', $this->base_path . '/src/' . $this->project . '/Layouts/gcwu.php');
        
        // Need to copy layout subfolders
        mkdir($this->base_path . '/src/' . $this->project . '/Layouts/gcwu');
        $sublayouts = glob($this->base_path . '/script/templates/views/gcwu/*.php');
        foreach ($sublayouts as $file) {
            copy($file, $this->base_path . '/src/' . $this->project . '/Layouts/gcwu/' . basename($file));
        }
        
        mkdir($this->base_path . '/src/' . $this->project . '/Layouts/bootstrap');
        $sublayouts = glob($this->base_path . '/script/templates/views/bootstrap/*.php');
        foreach ($sublayouts as $file) {
            copy($file, $this->base_path . '/src/' . $this->project . '/Layouts/bootstrap/' . basename($file));
        }
        
        mkdir($this->base_path . '/src/' . $this->project . '/Layouts/foundation');
        $sublayouts = glob($this->base_path . '/script/templates/views/foundation/*.php');
        foreach ($sublayouts as $file) {
            copy($file, $this->base_path . '/src/' . $this->project . '/Layouts/foundation/' . basename($file));
        }
        
        mkdir($this->base_path . '/src/' . $this->project . '/Layouts/gcweb');
        $sublayouts = glob($this->base_path . '/script/templates/views/gcweb/*.php');
        foreach ($sublayouts as $file) {
            copy($file, $this->base_path . '/src/' . $this->project . '/Layouts/gcweb/' . basename($file));
        }
        
        return $this->warning === null;
        
    }
    
    
    protected function createBaseExtension()
    {
        
        $base_template = file_get_contents($this->base_path . '/script/templates/extensions/base.php');
        $base_template = str_replace('{project}', $this->project, $base_template);
        return file_put_contents($this->base_path . '/src/' . $this->project . '/Views/Extensions/Base.php', $base_template);
    
    }
    
    protected function createAlertExtension()
    {
    
        $alerts_template = file_get_contents($this->base_path . '/script/templates/extensions/alerts.php');
        $alerts_template = str_replace('{project}', $this->project, $alerts_template);
        return file_put_contents($this->base_path . '/src/' . $this->project . '/Views/Extensions/Alerts.php', $alerts_template);
    
    }
    
    protected function copyAlertExtensionTemplates()
    {
        $copied = copy(
            $this->base_path . '/script/templates/extensions/alerts/templates.php', 
            $this->base_path . '/src/' . $this->project . '/Views/Extensions/Alerts/templates.php'
        );
        return $copied;
    }
    
    protected function copyLocales()
    {
        $en = copy(
            $this->base_path . '/script/templates/locales/en_CA.php',
            $this->base_path . '/src/' . $this->project . '/Locales/en_CA.php'
        );
        
        $fr = copy(
            $this->base_path . '/script/templates/locales/fr_CA.php',
            $this->base_path . '/src/' . $this->project . '/Locales/fr_CA.php'
        );
        
        return ($en === true && $fr === true);
    }
    
    protected function createDependencies()
    {
        $template = file_get_contents($this->base_path . '/script/templates/dependencies.php');
        $template = str_replace('{project}', $this->project, $template);
        $template = str_replace('{theme}', $this->theme, $template);
        $bytes = file_put_contents($this->base_path . '/src/' . $this->project . '/dependencies.php', $template);

        return $bytes !== false;
    }
    
    protected function createRoutes()
    {
        $template = file_get_contents($this->base_path . '/script/templates/routes.php');
        $template = str_replace('{project}', $this->project, $template);
        $bytes = file_put_contents($this->base_path . '/src/' . $this->project . '/routes.php', $template);
        
        return $bytes !== false;
    }
    
    protected function createMiddleware()
    {

        $copied = copy($this->base_path . '/script/templates/middleware.php', $this->base_path . '/src/' . $this->project . '/middleware.php');
        return $copied !== false;
    }
    
    protected function createDefaultController()
    {
        $template = file_get_contents($this->base_path . '/script/templates/controller.php');
        $template = str_replace('{project}', $this->project, $template);
        $template = str_replace('{class}', $this->controller, $template);
        $template = str_replace('{extends}', 'extends Controller', $template);
        $template = str_replace('{folder}', strtolower($this->controller), $template);
        return file_put_contents($this->base_path . '/src/'. $this->project . '/Controllers/' . $this->controller . '.php' , $template);
    }
    
    protected function createControllerViewFolder()
    {
        $view_folder = strtolower($this->controller);
        return mkdir($this->base_path . '/src/' . $this->project . '/Views/' . $view_folder);
        
    }
    
    protected function createControllerIndexView()
    {
        $view_folder = strtolower($this->controller);
        return copy($this->base_path . '/script/templates/views/index.php', $this->base_path . '/src/' . $this->project . '/Views/' . $view_folder . '/index.php');
    }

    
    
}
