<?php 
namespace Cli;

abstract class Base {
    
    const BUILD_STATUS_FAIL = 'FAIL';
    const BUILD_STATUS_OK = 'OK';
    protected $log = [];
    protected $base_path;
    protected $warning;
    protected $project;
    protected $controller;
    protected $theme;
    
    protected $build_status = self::BUILD_STATUS_FAIL;
    
    public function __construct($base, $project, $controller, $theme)
    {
        $this->base_path = $base;
        $this->project = ucfirst($project);
        $this->controller = ucfirst($controller);
        $this->theme = strtolower($theme);
        
        set_error_handler([$this, 'setWarning'], E_WARNING);
    }
    
    public function error($message)
    {
        $this->log[] = [
            'type'=>'error',
            'message'=>$message
        ];
    }
    
    public function info($message)
    {
        $this->log[] = [
            'type'=>'info',
            'message'=>$message
        ];
    }
    
    public function getBuildStatus()
    {
        return $this->build_status;
    }
    
    public function getLog()
    {
        return $this->log;
    }
    
    protected function setWarning($errno, $errstr)
	{
		$this->warning = $errstr;
	}
    
    protected function getWarning()
    {
        $warning = $this->warning;
        $this->warning = null;
        return $warning;
    }
}
